Feature: User can create a note
  Tests positive notes creation

  Scenario: Check if user can create a note
    Given Home page is displayed
    When Create a new note with title test and body test
    And User clicks back button
    Then Check if note test is expected to be available: true or false <zz>
