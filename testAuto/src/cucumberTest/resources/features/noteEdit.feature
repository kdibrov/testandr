Feature: User can create a note
  Tests positive notes creation

  Scenario: Check if user can create a note
    Given Home page is displayed
    When Create a new note with title note_title and body note_content
    Then Edit and check note with name note_title
