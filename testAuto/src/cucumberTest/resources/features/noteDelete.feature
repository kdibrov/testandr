Feature: User can delete a note
  Tests positive notes deletion

  Scenario: Check if user can delete a note
    Given Home page is displayed
    When Create a new note with title test_delete_title and body test_delete_content
    Then Delete note with name test_delete_title
    And Check if note test_delete_title is NOT found
