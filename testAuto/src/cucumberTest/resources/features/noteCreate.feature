Feature: User can create a note
  Tests positive notes creation

  Scenario: Check if user can create a note
    Given Home page is displayed
    When Create a new note with title test_title and body test_body
    Then Check if note test_title is available

