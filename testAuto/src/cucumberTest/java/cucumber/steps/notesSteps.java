package cucumber.steps;

import android_ui.Driver;
import android_ui.pages.HomePage;
import android_ui.pages.NotePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;


public class notesSteps extends Driver {
    private HomePage homePage = new HomePage(this.getDriver());
    private NotePage notePage = new NotePage(this.getDriver());

    @Given("^Home page is displayed$")
    public void checkHomePage() {
        if (!this.waitForPresence(1, homePage.searchButton)) {
            notePage.backButton.click();
        }
    }

    @When("^Create a new note with title (.*) and body (.*)")
    public void createNewNote(String title, String body) {
        checkHomePage();
        homePage.plusButton.click();
        notePage.FillNoteDetails(title, body, false);
        notePage.clickBackButton();
        this.waitForPresence(1, homePage.searchButton);
    }

    @Then("Check if note (.*) is available")
    public void checkNoteIsAvailable(String noteName) {
        WebElement noteElement = this.ReturnElementByXpathText(noteName);
        assert this.waitForPresence(1, noteElement);
    }

    @Then("^Edit and check note with name (.*)")
    public void EditAndCheckNote(String noteName) {
        WebElement noteElement = this.ReturnElementByXpathText(noteName);
        noteElement.click();
        notePage.FillNoteDetails("edit_" + noteName, "edit_content", true);
        notePage.clickBackButton();
        WebElement editedNoteElement = this.ReturnElementByXpathText("edit_" + noteName);
        this.waitForPresence(1, editedNoteElement);
    }

    @When("^Delete note with name (.*)")
    public void CreateAndDeleteNote(String noteName) {
        WebElement noteElement = this.ReturnElementByXpathText(noteName);
        noteElement.click();
        this.waitForPresence(1, notePage.deleteNoteButton);
        notePage.deleteNote();
        this.waitForPresence(1, homePage.searchButton);
    }

    @Then("Check if note (.*) is NOT found")
    public void checkDeletedNote(String noteName) {
        assert !this.CheckElementPresenseByText(noteName);
    }
}