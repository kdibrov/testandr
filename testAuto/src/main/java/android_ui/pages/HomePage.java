package android_ui.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;

import static java.util.concurrent.TimeUnit.SECONDS;


public class HomePage {

    public HomePage(AndroidDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver, 30, SECONDS), this);
    }

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/fab_button")
    public AndroidElement plusButton;

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/action_search")
    public AndroidElement searchButton;

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/action_search")
    public AndroidElement searchField;

    @CacheLookup
    @AndroidFindBy(xpath = "//*[@text='No saved note']")
    public AndroidElement noNotesLabel;

    public HomePage searchForNote(String searchText) {
        searchButton.click();
        searchField.sendKeys(searchText + "\n");
        return this;
    }
}