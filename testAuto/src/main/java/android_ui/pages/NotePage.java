package android_ui.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;

import static java.util.concurrent.TimeUnit.SECONDS;


public class NotePage {

    public NotePage(AndroidDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver, 30, SECONDS), this);
    }

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/et_title")
    public AndroidElement noteTitle;

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/et_content")
    public AndroidElement noteContent;

    @CacheLookup
    @AndroidFindBy(id = "com.evenwell.android.memo:id/action_delete")
    public AndroidElement deleteNoteButton;

    @CacheLookup
    @AndroidFindBy(xpath = "//*[@text='DELETE']")
    public AndroidElement deleteNoteButtonConfirm;

    @CacheLookup
    @AndroidFindBy(accessibility = "Navigate up")
    public AndroidElement backButton;


    public void FillNoteDetails(String title, String content, boolean clearNote) {
        if (clearNote) {
            noteContent.clear();
            noteTitle.clear();
        }
        noteTitle.setValue(title);
        noteContent.setValue(content);
    }

    public void clickBackButton() {
        backButton.click();
    }

    public void deleteNote() {
        deleteNoteButton.click();
        deleteNoteButtonConfirm.click();
    }

}