package android_ui;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Driver {

    public Driver() {
        initDriver();
    }

    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private AndroidDriver androidDriver;

    protected void setup(){
        initDriver();
    }

    protected AndroidDriver getDriver() {
        return androidDriver;
    }

    private void initDriver(){
        capabilities.setCapability("deviceName", "Pixel API 26");
        capabilities.setCapability("platformVersion", "8.0");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("app", "/Users/kostyantyn.dibrov/Downloads/notes.apk");
        System.out.println("Desired Capabilities:\n" + capabilities.toString());
        try
        {
            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        }
        catch (NullPointerException | MalformedURLException ex) {
            throw new RuntimeException("Appium driver could not be initialised");
        }
    }

    protected boolean waitForPresence(int timeLimitInSeconds, WebElement webElement) {
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), timeLimitInSeconds);
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return webElement.isDisplayed();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    protected WebElement ReturnElementByXpathText(String text) {
        String xpath =  String.format("//*[@text='%s']", text);
        return (AndroidElement) getDriver().findElement(By.xpath(xpath));
    }

    protected boolean CheckElementPresenseByText(String noteName) {
        String xpath =  String.format("//*[@text='%s']", noteName);
        try {
            getDriver().findElementByXPath(xpath);
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Element is not expected to be found");
            return false;
        }
    }

    protected void tearDown(){
        androidDriver.quit();
    }
}